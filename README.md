## How to use this repo

To use cdtools, each shell function has a file under ~/bin/env.  See src/env/sample for an example.

- Create directories

	    mkdir -p bin/env

- Copy src/cdtools to ~/bin
- Add it to your .bashrc:

	    echo ". ~/bin/cdtools" > ~/.bashrc

- Copy src/env/sample to ~/bin/env/<your project> and edit as appropriate
- Logout, login.
- Run your script's function name
- See how to use your repo

	    cd?

- See list of available projects

	    cdlist

That's about it.

## License

0BSD
